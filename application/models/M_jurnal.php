<?php
class M_jurnal extends CI_Model
{

    function get_jurnal_all()
    {
        $query = $this->db->get('jurnal');
        return $query->result_array();
    }

    public function insert_jurnal($data)
    {
        $this->db->insert('jurnal', $data);
        return TRUE;
    }

    public function update_jurnal($data, $data_id)
    {
        $this->db->update('jurnal', $data, $data_id);
        return TRUE;
    }

    function delete_jurnal($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_all_jurnal()
    {
        $this->db->select('*');
        $this->db->from('jurnal');
        $this->db->order_by('jurnal_id', 'DESC');
        return $this->db->get();
    }

    public function getDataPagination($limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('jurnal');
        $this->db->order_by('jurnal_id', 'DESC');
        $this->db->limit($limit, $offset);

        return $this->db->get();
    }

    public function getDataPagination_old($limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('jurnal');
        $this->db->order_by('jurnal_id', 'ASC');
        $this->db->limit($limit, $offset);

        return $this->db->get();
    }
}
