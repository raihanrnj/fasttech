<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_statistik extends CI_Model {

	public function total_anggota() {
		$data = $this->db->get('anggota');

		return $data->num_rows();
	}


	public function total_mahasiswa() {

		$this->db->where('profesi', 'mahasiswa');
		$data = $this->db->get('anggota');
		return $data->num_rows();
	}

	public function total_pengJurnal() {
		$this->db->where('profesi', 'pengelola_jurnal');
		$data = $this->db->get('anggota');
		return $data->num_rows();
	}
}

/* End of file M_posisi.php */
/* Location: ./application/models/M_posisi.php */