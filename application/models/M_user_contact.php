<?php

class M_user_contact extends CI_Model
{
    function get_contact()
    {
        $query = $this->db->get('u_contact');
        return $query->result_array();
    }

    public function insert($data)
    {
        $this->db->insert('u_contact', $data);
        return TRUE;
    }

    public function update($data, $data_id)
    {
        $this->db->update('u_contact', $data, $data_id);
        return TRUE;
    }

    function delete_contact($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
