<?php
class M_user_pengurus extends CI_Model
{

    function get_pengurus_all()
    {
        $query = $this->db->get('u_pengurus');
        return $query->result_array();
    }

    public function insert_pengurus($data)
    {
        $this->db->insert('u_pengurus', $data);
        return TRUE;
    }

    public function update_pengurus($data, $id)
    {
        $this->db->update('u_pengurus', $data, $id);
        return TRUE;
    }

    function delete_pengurus($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_provinsi()
    {
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('provinces');
        return $query->result();
    }

    function get_data_chart()
    {
        $query = $this->db->query("SELECT kode AS id, kolom AS 'column', nama AS 'name', CONCAT(`afiliasi`, '<br>      ', `alamat`)  AS description, deskripsi AS 'title', foto AS image FROM u_pengurus");
        return $query->result_array();
    }

    function get_desc_pengurus_all()
    {
        $query = $this->db->get('pengurus_desc');
        return $query->result_array();
    }

    public function insert_deskripsi($data)
    {
        $this->db->insert('pengurus_desc', $data);
        return TRUE;
    }

    public function update_deskripsi($data, $id)
    {
        $this->db->update('pengurus_desc', $data, $id);
        return TRUE;
    }

    function delete_deskripsi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
