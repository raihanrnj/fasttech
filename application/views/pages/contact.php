<main>
   <div class="slider-area">
      <div class="single-sliders slider-height2 d-flex align-items-center" style="margin-top:25px;">
         <div class="container">
            <div class="row align-items-center">
               <!-- <div class="row"> -->
                  <div class="col-lg-6 col-md-6 col-sm-6" style="margin-top:5%;">
                     <div class="hero-caption hero-caption2">
                        <h2>Contact Us</h2>
                       <p> Get Our Fast Services</p>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="margin-top: 15%;">
                      <img src="<?= base_url() ?>assets/images/contact-page-banner.png" alt="" style="height: 290px;">
                  </div>
               <!-- </div> -->
            </div>
         </div>
      </div>
   </div>
   <section class="contact-section section-padding">
         <div class="row" style="margin-left:10%;margin-right:10%;">
            <div class="col-12">
               <h2 class="contact-title">Get in Touch</h2>
            </div>
            <div class="col-lg-8">
               <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                  <div class="row">
                     <div class="col-12">
                        <div class="form-group">
                           <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"></textarea>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email">
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="form-group">
                           <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject">
                        </div>
                     </div>
                  </div>
                  <div class="form-group mt-3">
                     <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                  </div>
               </form>
            </div>
            <div class="col-lg-3 offset-lg-1">
               <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-home"></i></span>
                  <div class="media-body">
                     <h3>Jakarta Selatan, DKI Jakarta</h3>
                     <p>Jl. Prof.dr.Soepomo No. 178. Menteng Dalam. 12870</p>
                  </div>
               </div>
               <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                  <div class="media-body">
                     <h3>+62 856 9458 0598</h3>
                     <p>Mon to Fri 9am to 5pm</p>
                  </div>
               </div>
               <div class="media contact-info">
                  <span class="contact-info__icon"><i class="ti-email"></i></span>
                  <div class="media-body">
                     <h3>admin@fasttechid.com</h3>
                     <p>Send us your query anytime!</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>