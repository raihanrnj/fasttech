<main>
   <!-- <div class="slider-area">
      <div class="single-sliders slider-height2 d-flex align-items-center">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-xl-5 col-lg-6 col-md-6">
                  <div class="hero-caption hero-caption2">
                     <h2>Services</h2>
                     <p>IT solutions for startup and enterprises</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div> -->
   <section class="categories-area section-padding" style="margin-top:25px;">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-10">
               <div class="section-tittle text-center mb-35">
                  <span>Our Services</span>
                  <h2>IT solutions for startup and enterprises</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services1.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Mobile App Solutions</a></h5>
                     <p>We offers mobile apps solutions for businesses of all sizes.</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services2.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Web Solutions</a></h5>
                     <p>We Help You Build Your Dream Website, for your business, company or any of kind.</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services3.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Manage IT Services</a></h5>
                     <p>Managed IT Services are made up of a team of highly experienced resources. </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services1.svg" alt="">
                  </div>
             
                  <div class="cat-cap">
                     <h5><a href="services.html">UI/UX Design Services</a></h5>
                     <p>We provide qualified resources that create web and mobile user interfaces.</p>
       
                  </div>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services2.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">IT Training and Course</a></h5>
                     <p>Grow your business With trained resources.</p>
                  </div>
               </div>
            </div>
            <!-- <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<= base_url() ?>assets/images/icon-services3.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Software House</a></h5>
                     <p>Naxly bring the power of data science and artificial intelligence to every business.</p>
                  </div>
               </div>
            </div> -->
         </div>
      </div>
   </section>