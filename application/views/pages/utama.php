<main>
   <div class="slider-area  ">
      <div class="single-sliders slider-height   d-flex align-items-center" style="margin-top:25px;">
         <div class="container position-relative">
            <div class="row align-items-center">
               <div class="col-xl-5 col-lg-6 col-md-6">
                  <div class="hero-caption">
                     <h1>Your IT Business Partner</h1>
                     <p>Fasttech has trusted partner in service clientele from, government agencies, global insurance company & large FMCG manufacturer etc.</p>
                     <a href="<?= base_url() ?>services" class="btn hero-btn">Our Services</a>
                  </div>
               </div>
               <div class="hero-shape">
                  <img src="<?= base_url() ?>assets/images/hero-hero-img.png" alt="">
               </div>
            </div>
         </div>
      </div>
   </div>
   <section class="categories-area section-padding">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-10">
               <div class="section-tittle text-center mb-35">
                  <span>Our Services</span>
                  <h2>IT solutions for startup and enterprises</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services1.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Mobile App Solutions</a></h5>
                     <p>We offers mobile apps solutions for businesses of all sizes.</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services2.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Web Solutions</a></h5>
                     <p>We Help You Build Your Dream Website, for your business, company or any of kind.</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services3.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">Manage IT Services</a></h5>
                     <p>Managed IT Services are made up of a team of highly experienced resources. </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services1.svg" alt="">
                  </div>
             
                  <div class="cat-cap">
                     <h5><a href="services.html">UI/UX Design Services</a></h5>
                     <p>We provide qualified resources that create web and mobile user interfaces.</p>
       
                  </div>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
               <div class="single-cat text-center mb-50">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-services2.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="services.html">IT Training and Course</a></h5>
                     <p>Grow your business With trained resources.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="about-area2 section-padding gray-bg ">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-xl-5 col-lg-5 col-md-10">
               <div class="about-img">
                  <img src="<?= base_url() ?>assets/images/gallery-about1.jpg" alt="">
               </div>
               <!-- <div class="ratting-point">
                  <div class="features-ratting">
                     <img src="<= base_url() ?>assets/images/icon-notification.svg" alt="">
                  </div>
                  <div class="features-caption">
                     <h3>New Features</h3>
                     <p>You have received notification</p>
                  </div>
               </div> -->
            </div>
            <div class="col-xl-5 col-lg-6 col-md-10">
               <div class="about-caption mb-50">
                  <div class="section-tittle mb-50">
                     <h2>Our Key <br> Performances</h2>
                  </div>
                  <div class="single-offers mb-20">
                     <div class="offers-cap">
                        <img src="<?= base_url() ?>assets/images/icon-notification1.svg" alt="">
                        <h3><a href="#">Reliable</a></h3>
                        <p>100% Fulfillment of needs for our clients</p>
                     </div>
                  </div>
                  <div class="single-offers">
                     <div class="offers-cap">
                        <img src="<?= base_url() ?>assets/images/icon-notification1.svg" alt="">
                        <h3><a href="#">3 CV'S</a></h3>
                        <p>Minimum submitted Curriculum Vitaes per week</p>
                     </div>
                  </div>
                  <a href="<?= base_url() ?>about" class="btn mt-20">Learn More</a>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="services-area2 section-padding2">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-11">
               <div class="section-tittle section-tittle2 mb-55">
                  <span>Industries We Serve</span>
                  <h2>IT services customized for your industry</h2>
               </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-11">
               <div class="section-tittle section-tittle2 mb-55">
                  <p>No matter the business, Fasttech.id has you covered with industry compliant solutions, customized to your company&rsquo;s specific needs.</p>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
               <div class="single-cat mb-30 text-center">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-stack1.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="#">Industries &amp; Manufacturing</a></h5>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
               <div class="single-cat mb-30 text-center">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-stack2.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="#">Education &amp; Health</a></h5>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
               <div class="single-cat mb-30 text-center">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-stack3.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="#">Retail &amp; Ecommerce</a></h5>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
               <div class="single-cat mb-30 text-center">
                  <div class="cat-icon">
                     <img src="<?= base_url() ?>assets/images/icon-stack4.svg" alt="">
                  </div>
                  <div class="cat-cap">
                     <h5><a href="#">Travel &amp; Hospitality</a></h5>
                  </div>
               </div>
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-xl-12">
               <div class="section-tittle section-tittle3 text-center pt-35">
                  <p>Need a Technology support? <a href="<?= base_url() ?>contact">Get Started Now</a></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <div class="count-down-area section-padding border-bottom">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-9  col-md-12">
               <div class="section-tittle text-center mb-60">
                  <h2>Fasttech.ID - Partner for Your Business </h2>
                  <p class="mb-55">We understand the complexities of modern markets and translate them into real business solutions for automotive, financial, insurance, pharma &amp; life sciences, and real estate with more to come.</p>
                  <!-- <a href="#" class="border-btn">Request a Quote</a> -->
               </div>
            </div>
         </div>
         <div class="row justify-content-center ">
            <div class="col-lg-12 col-md-12">
               <div class="count-down-wrapper">
                  <div class="row justify-content-between">
                     <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                        <div class="single mb-30">
                           <div class="single-counter text-center">
                              <span class="counter ">15</span>
                              <p class="">+</p>
                           </div>
                           <div class="pera-count  text-center">
                              <p>Company Partners</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                        <div class="single mb-30">
                           <div class="single-counter text-center">
                              <span class="counter ">60</span>
                              <p class="">+</p>
                           </div>
                           <div class="pera-count text-center">
                              <p>Professional Talents</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                        <div class="single mb-30">
                           <div class="single-counter text-center">
                              <span class="counter ">3</span>
                              <p class="">+</p>
                           </div>
                           <div class="pera-count text-center">
                              <p>Country Partners</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- <div class="great-stuffs section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="section-tittle text-center mb-55">
                  <h2>Some of Our Great Stuffs</h2>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
               <div class="single-location mb-30">
                  <div class="location-img">
                     <img src="<= base_url() ?>assets/images/gallery-xstuffs1.jpg.pagespeed.ic.FEEye3ibe4.jpg" alt="">
                     <div class="location-details text-center">
                        <span class="location-btn"></span>
                        <h4>inspire</h4>
                        <p>design studio</p>
                        <img src="<= base_url() ?>assets/images/icon-icon1.svg" alt="">
                     </div>
                  </div>
                  <h3><a href="#">The Security Risks of Changing Package Owners</a></h3>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
               <div class="single-location mb-30">
                  <div class="location-img">
                     <img src="<= base_url() ?>assets/images/gallery-xstuffs2.jpg.pagespeed.ic.hQ-J9pRQFV.jpg" alt="">
                     <div class="location-details text-center">
                        <span class="location-btn"></span>
                        <h4>inspire</h4>
                        <p>design studio</p>
                        <img src="<= base_url() ?>assets/images/icon-icon1.svg" alt="">
                     </div>
                  </div>
                  <h3><a href="#">The Security Risks of Changing Package Owners</a></h3>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
               <div class="single-location mb-30">
                  <div class="location-img">
                     <img src="<= base_url() ?>assets/images/gallery-xstuffs3.jpg.pagespeed.ic.ZeSX6nQVr4.jpg" alt="">
                     <div class="location-details text-center">
                        <span class="location-btn"></span>
                        <h4>inspire</h4>
                        <p>design studio</p>
                        <img src="<= base_url() ?>assets/images/icon-icon1.svg" alt="">
                     </div>
                  </div>
                  <h3><a href="#">The Security Risks of Changing Package Owners</a></h3>
               </div>
            </div>
         </div>
      </div>
   </div> -->
   <section class="collection section-img-bg2  section-over1" data-background="assets/img/gallery/section_bg01.jpg">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-9">
               <div class="single-question text-center">
                  <div class="video-icon mb-40">
                     <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0" data-animation="bounceIn" data-delay=".4s"><i class="fas fa-play"></i></a>
                  </div>
                  <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s">Innovative IT Solution for your Business &amp; Startup</h2>
                  <a href="about.html" class="white-btn wow fadeInUp" data-wow-duration="2s" data-wow-delay=".4s">Get Started Now</a>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
