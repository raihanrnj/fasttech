
<footer>
   <div class="footer-wrapper">
      <div class="footer-area footer-padding">
         <div class="container">
            <div class="row justify-content-between">
               <div class="col-xl-4 col-lg-3 col-md-6 col-sm-8">
                  <div class="single-footer-caption mb-50">
                     <div class="single-footer-caption mb-30">
                        <div class="footer-logo mb-35">
                           <a href="index.html">
                              <!-- <img src="<?= base_url() ?>assets/images/logo-xlogo2_footer.png.pagespeed.ic.XMcXopKKpv.png" alt=""> -->
                              <img src="<?= base_url() ?>assets/images/logo-fasttech.png" alt="" style="height: 80px;">
                           </a>
                        </div>
                        <div class="footer-tittle">
                           <div class="footer-pera">
                              <p>GET OUR FAST SERVICES </p>
                           </div>
                        </div>
                        <ul class="mb-20">
                           <li class="number"><a href="#">+62 856 9458 0598</a></li>
                           <li class="number2"><a href="#"><span class="__cf_email__" >admin@fasttechid.com</span></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                  <div class="single-footer-caption mb-50">
                     <div class="footer-tittle">
                        <h4>Navigation</h4>
                        <ul>
                           <li><a href="<?= base_url() ?>">Home</a></li>
                           <li><a href="<?= base_url() ?>about">About</a></li>
                           <li><a href="<?= base_url() ?>services">Services</a></li>
                           <!-- <li><a href="#">Blog</a></li> -->
                           <li><a href="<?= base_url() ?>contact">Contact</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                  <div class="single-footer-caption mb-50">
                     <!-- <div class="footer-tittle">
                        <h4>Services</h4>
                        <ul>
                           <li><a href="#">Drone Mapping</a></li>
                           <li><a href="#"> Real State</a></li>
                           <li><a href="#">Commercial</a></li>
                           <li><a href="#">Construction</a></li>
                        </ul>
                     </div> -->
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-8">
                  <div class="single-footer-caption mb-50">
                     <div class="footer-tittle mb-10">
                        <h4>Subscribe newsletter</h4>
                        <p>Subscribe our newsletter to get updates about our services and offers.</p>
                     </div>
                     <div class="footer-form mb-20">
                        <div id="mc_embed_signup">
                           <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" class="subscribe_form relative mail_part">
                              <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address" class="placeholder hide-on-focus" onfocus="this.placeholder = ''" onblur="this.placeholder = ' Email Address '">
                              <div class="form-icon">
                                 <button type="submit" name="submit" id="newsletter-submit" class="email_icon newsletter-submit button-contactForm">
                                 <i class="fas fa-arrow-right"></i>
                                 </button>
                              </div>
                              <div class="mt-10 info"></div>
                           </form>
                        </div>
                     </div>
                     <div class="footer-social">
                        <a href="#"><i class="fab fa-facebook"></i></a>
                        <a href="https://www.instagram.com/fasttech_id/?hl=id"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.linkedin.com/in/fasttechid/"><i class="fab fa-linkedin-in"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-bottom-area">
         <div class="container">
            <div class="footer-border">
               <div class="row">
                  <div class="col-xl-12 ">
                     <div class="footer-copy-right text-center">
                        <p>
                           Copyright &copy;<script data-cfasync="false" src="<?= base_url() ?>assets/js/cloudflare-static-email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | Fasttech.ID
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>
<div id="back-top">
   <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div>

<script src="<?= base_url() ?>assets/js/vendor-modernizr-3.5.0.min.js"></script>
<script src="<?= base_url() ?>assets/js/vendor-jquery-1.12.4.min.js"></script>
<script src="<?= base_url() ?>assets/js/js-popper.min.js+bootstrap.min.js.pagespeed.jc.9cRULW2NGb.js"></script><script>eval(mod_pagespeed_crWFRqXdFy);</script>
<script>eval(mod_pagespeed_ijZOghDUX8);</script>

<script src="<?= base_url() ?>assets/js/js-owl.carousel.min.js+slick.min.js.pagespeed.jc.zQmc_a1Fv1.js"></script><script>eval(mod_pagespeed_PzgEIv5PcB);</script>
<script>eval(mod_pagespeed_xIW8NoJlxN);</script>
<script src="<?= base_url() ?>assets/js/js-jquery.slicknav.min.js+wow.min.js+jquery.magnific-popup.js+jquery.nice-select.min.js+counterup.min.js+waypoints.min.js+contact.js.pagespeed.jc.tRMmUKDoDC.js"></script><script>eval(mod_pagespeed_pv626cP4z7);</script>

<script>eval(mod_pagespeed_Kky13b0x2I);</script>
<script>eval(mod_pagespeed_EDuJ3QEkVk);</script>
<script>eval(mod_pagespeed_kgpL3f31nU);</script>
<script>eval(mod_pagespeed_lJhlYOvYxQ);</script>
<script>eval(mod_pagespeed_oAlhDyvEqW);</script>

<script>eval(mod_pagespeed_PFgx0hSwwQ);</script>
<script src="<?= base_url() ?>assets/js/js-jquery.form.js+jquery.validate.min.js+mail-script.js+jquery.ajaxchimp.min.js+plugins.js+main.js.pagespeed.jc.HTi8r6WLOP.js"></script><script>eval(mod_pagespeed_h4t9Jzj872);</script>
<script>eval(mod_pagespeed_wxtAYWpZWb);</script>
<script>eval(mod_pagespeed_AdrT36UReh);</script>
<script>eval(mod_pagespeed_Fc9noQyPX5);</script>

<script>eval(mod_pagespeed_o4EKzsn7hM);</script>
<script>eval(mod_pagespeed_ilK$maA0q5);</script>

<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script defer src="<?= base_url() ?>assets/js/beacon.min.js" data-cf-beacon='{"rayId":"686f993d8b06090d","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.8.1","si":10}'></script>
</body>
</html>
