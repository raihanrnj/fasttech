<!DOCTYPE html>
<html class="no-js" lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Fasttech.ID</title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>assets/images/logo-fasttech.png">
      <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
      <link rel="stylesheet" href="<?= base_url() ?>assets/css/second_style.css">
   </head>
   <body>
      <header>
         <div class="header-area header-transparent">
            <div class="main-header header-sticky">
               <div class="container">
                  <div class="row">
                     <div class="col-xl-12">
                        <div class="menu-wrapper d-flex align-items-center justify-content-between">
                           <div class="left-content d-flex align-items-center">
                              <div class="logo mr-30">
                                 <a href="<?= base_url() ?>">
                                 <!-- <img src="<?= base_url() ?>assets/images/logo-fasttech.png" alt=""> -->
                                 <img src="<?= base_url() ?>assets/images/logo-fasttech.png" alt="" style="height: 60px;">
                                </a>
                              </div>
                              <div class="main-menu d-none d-lg-block">
                                 <nav>
                                    <ul id="navigation">
                                       <li><a href="<?= base_url() ?>">Home</a></li>
                                       <li><a href="<?= base_url() ?>services">Services</a></li>
                                       <li><a href="<?= base_url() ?>about">About</a></li>
                                       <!-- <li><a href="case_study.html">Case Study</a></li> -->
                                       <!-- <li>
                                          <a href="#">Blog</a>
                                          <ul class="submenu">
                                             <li><a href="blog.html">Blog</a></li>
                                             <li><a href="blog_details.html">Blog Details</a></li>
                                             <li><a href="elements.html">Element</a></li>
                                          </ul>
                                       </li> -->
                                       <li><a href="<?= base_url() ?>contact">Contact</a></li>
                                    </ul>
                                 </nav>
                              </div>
                           </div>
                           <div class="buttons">
                              <ul>
                                 <li class="button-header">
                                    <a href="https://api.whatsapp.com/send?text=HalloFastech&phone=+6285694580598" class="header-btn2 border-btn2">Join Us</a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="mobile_menu d-block d-lg-none"></div>
                  </div>
               </div>
            </div>
         </div>
      </header>