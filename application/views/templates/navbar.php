<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8" />
    <title> Imaji | <?php echo $page = ucwords(str_replace('_', ' ', $page)) ?>
    </title>

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="One page parallax responsive HTML Template" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0" />
    <meta name="author" content="Imaji" />
    <meta name="generator" content="Imaji WEB" />

    <!-- Favicon -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" /> -->
    <link rel="shortcut icon" type="image/jpg" href="<?= base_url() ?>assets/images/logo.png" />

    <!-- CSS ================================================== -->

    <!-- Themefisher Icon font -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/themefisher-font/style.css" />
    <!-- font-awesome icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/bootstrap.min.css" />
    <!-- Lightbox.min css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/lightbox2/css/lightbox.min.css" />
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/animate/animate.css" />
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/slick/slick.css" />
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" />
    <!-- treeMaker Stylesheet -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/treeMaker/tree_maker-min.css" />
    <!-- daterangepicker Stylesheet -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/page/<?= strtolower($page) ?>.css">
</head>

<body id=" body">


    <!-- Fixed Navigation ==================================== -->
    <header class="navigation fixed-top sticky-header">
        <div class="container">
            <!-- main nav -->
            <nav class="navbar navbar-expand-lg navbar-light px-0">
                <!-- logo -->
                <a class="navbar-brand logo" href="<?= base_url() ?>">
                    <img loading="lazy" class="logo-default" src="<?= base_url() ?>assets/images/Logo-IMAJI.gif" alt="logo" width="250px" />
                    <img loading="lazy" class="logo-white" src="<?= base_url() ?>assets/images/Logo-IMAJI.gif" alt="logo" width="250px" />
                </a>
                <!-- /logo -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navigation">
                    <ul class="navbar-nav ml-auto text-center">
                        <li class="nav-item dropdown active">
                        </li>
                        <li class="nav-item <?php if ($this->uri->segment(3) == 'profiles') {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?= base_url() ?>page/view/profiles">Profile</a>
                        </li>

                        <li class="nav-item <?php if ($this->uri->segment(3) == 'pendiri' || $this->uri->segment(3) == 'pengurus') {
                                                echo "active";
                                            } ?> dropdown">
                            <a class="nav-link dropdown-toggle" href="#!" id="navbarDropdown02" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Organisasi <i class="tf-ion-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown02">
                                <li><a class="dropdown-item <?php if ($this->uri->segment(3) == 'pendiri') {
                                                                echo "active";
                                                            } ?>" href="<?= base_url() ?>page/view/pendiri">Pendiri</a></li>
                                <li>
                                    <a class="dropdown-item <?php if ($this->uri->segment(3) == 'pengurus') {
                                                                echo "active";
                                                            } ?>" href="<?= base_url() ?>page/view/pengurus">Pengurus</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item <?php if ($this->uri->segment(3) == 'berita' || $this->uri->segment(3) == 'berita_old') {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?= base_url() ?>page/view/berita">Berita</a>
                        </li>
                        <li class="nav-item <?php if ($this->uri->segment(3) == 'keanggotaan') {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?= base_url() ?>page/view/keanggotaan">Keanggotaan</a>
                        </li>
                        <li class="nav-item <?php if ($this->uri->segment(3) == 'jurnal' || $this->uri->segment(3) == 'jurnal_old') {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?= base_url() ?>page/view/jurnal">Publikasi</a>
                        </li>
                        <li class="nav-item <?php if ($this->uri->segment(3) == 'kontak') {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?= base_url() ?>page/view/kontak">Kontak</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url() ?>auth">login</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- /main nav -->
        </div>
    </header>